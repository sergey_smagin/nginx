#!/bin/bash

NO_ARGS=0
CONFIG_PATH=./sites-available/
WORK_DIR=./sites-enabled/
ALFRESCO=true
TMPL=./nginx_default.tmpl
SCHEME=http
SSL_CONF=/etc/nginx/default.d/citeck.ru.ssl.conf


usage() {
    echo "Usage: ./config-gen.sh -n example.com -a 127.0.0.1 -p 8080 -d ecos24.ru -s https"
    echo "Options:"
    echo "    -n servername"
    echo "    -a address for proxy pass"
    echo "    -p port for proxy pass"
    echo "    -x Create a configuration not for Alfresco. By default, the configuration file is created for Alfresco services."
    echo "    -d Select a domain for certificate certificate citeck.ru, or citeck.com, or ecos24.ru. The default is citeck.ru."
    echo "    -s Сhoice of HTTP or HTTPS scheme. HTTP is used by default."
    echo "    -h print this help"
}

generate() {
    if ! [[ -d $CONFIG_PATH ]]; then
      echo "$CONFIG_PATH not exist"
      exit 1
    elif ! [[ -d $WORK_DIR ]]; then
      echo "$WORK_DIR not exist"
      exit 1
    elif ! [[ -f $TMPL ]]; then
      echo "$TMPL not exist"
      exit 1
    fi

    if  [[ "$DOMAIN" == "citeck.com" ]] ; then
      SSL_CONF=/etc/nginx/default.d/citeck.com.ssl.conf
      echo "$SSL_CONF"
    elif [[ "$DOMAIN" == "ecos24.ru" ]] ; then
      SSL_CONF=/etc/nginx/default.d/ecos.ssl.conf
      echo "$SSL_CONF"
    fi

    sed "s!{{nginx.proxypassport}}!$PROXY_PORT!g" $TMPL | \
    sed "s!{{nginx.hostname}}!$SERVER_NAME!g" | \
    sed "s!{{nginx.proxypassaddress}}!$PROXY_ADDR!g" | \
    sed "s!{{nginx.sslconf}}!$SSL_CONF!g" | \
    sed "s!{{nginx.scheme}}!$SCHEME!g"  > /tmp/temp_nginx.conf

    if [[ -n $ALFRESCO ]]; then
#      echo "Create a configuration for Alfresco"
      sed '/#BEGIN_DEFAULT/,/#END_DEFAULT/d; / *#/d' /tmp/temp_nginx.conf > "$CONFIG_PATH"/"$SERVER_NAME".conf
      rm -f /tmp/temp.conf
    else
#      echo "Create a configuration not for Alfresco"
      sed '/#BEGIN_ALFRESCO/,/#END_ALFRESCO/d; / *#/d' /tmp/temp_nginx.conf > "$CONFIG_PATH"/"$SERVER_NAME".conf
      rm -f /tmp/temp.conf
    fi
    ln -s "$CONFIG_PATH"/"$SERVER_NAME".conf "$WORK_DIR"/"$SERVER_NAME".conf
    nginx -s reload
}


if [ $# -eq "$NO_ARGS" ]
then
  usage
  exit $E_OPTERROR
fi

while getopts ":hxa:p:n:d:s:" opt; do
  case $opt in
    a)
      PROXY_ADDR="$OPTARG" >&2
      ;;
    n)
      SERVER_NAME="$OPTARG" >&2
      ;;
    p)
      PROXY_PORT="$OPTARG" >&2
      ;;
    x)
      ALFRESCO=""
      ;;
    d)
      DOMAIN="$OPTARG" >&2
      ;;
    s)
      SCHEME="$OPTARG" >&2
      ;;
    h)
      usage
      exit 1
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
    *)
      usage
      exit $E_OPTERROR
      ;;
  esac
done


generate

